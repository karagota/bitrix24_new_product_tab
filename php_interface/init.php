<?
//Подключение своих CSS и JS к Битрикс24

use Bitrix\Main\EventManager;
EventManager::getInstance()->addEventHandler(
    'main',
    'OnEpilog',
    [MyEvents::class, 'OnEpilog']
);

EventManager::getInstance()->addEventHandler(
    'main',
    'OnProlog',
    [MyEvents::class, 'OnProlog']
);

class MyEvents
{
    public static function OnProlog()
    {
        global $USER;
        $arJsConfig = array(
            'custom_start'=>array(
                'js'=>'/local/additional/main.js',
                'css'=>'/local/additional/main.css',
                'rel'=>array()
            )
        );
        foreach ($arJsConfig as $ext => $arExt) {
            \CJSCore::RegisterExt($ext, $arExt);
        }
        CUtil::InitJSCore(array('custom_start'));

    }

    public static function OnEpilog()
    {


    }


    public static function OnBeforePrologHandler()
    {
        CJSCore::Init(array('jquery2'));
    }

}
?>