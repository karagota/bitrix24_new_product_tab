<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Bizproc\Activity\BaseActivity;
use Bitrix\Bizproc\FieldType;
use \Bitrix\Main\ErrorCollection;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Bizproc\Activity\PropertiesDialog;


class CBPDadataActivity extends BaseActivity
{
    /**
     * @see parent::__construct()
     * @param $name string Activity|none
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->arProperties = [
            'INN' => '',
            //return
            'CompanyName' => null,
        ];

        $this->SetPropertiesTypes([
            'CompanyName' => ['Type' => FieldType::STRING],
        ]);
    }

    /**
     * Return activity file path
     * @return string
     */
    protected static function getFileName(): string
    {
        return __FILE__;
    }

    /**
     * @return ErrorCollection
     */
    protected function internalExecute(): ErrorCollection
    {
        $errors= parent::internalExecute();
        require_once(__DIR__."/vendor/autoload.php");
        $token = '___SOME__TOKEN__';
        $secret = '___SOME__SECRET__';
        $dadata = new \Dadata\DadataClient($token, $secret);
        $response = $dadata->findById("party", $this->INN);
        if (isset($response[0]['value'])) {
            $this->preparedProperties['CompanyName'] = $response[0]['value'];
            $this->log($this->preparedProperties['CompanyName']);
        }
        return $errors;

    }

    /**
     * @param PropertiesDialog|null $dialog
     * @return array[]
     */
    public static function getPropertiesDialogMap(?PropertiesDialog $dialog = null): array
    {
        $map = [
            'INN' => [
               'Name' => Loc::getMessage('DADATA_ACTIVITY_FIELD_INN'),
               'FieldName' => 'inn',
               'Type' => FieldType::STRING,
               'Required' => true,
               'Default' => '',
               'Options' => [],
            ]
        ];
        return $map;
    }
}