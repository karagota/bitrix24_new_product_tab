<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$MESS['DADATA_DESCR_NAME'] = 'Получение названия компании по ИНН';
$MESS['DADATA_DESCR_DESCR'] = 'Получение названия компании по ИНН из Dadata';
$MESS['DADATA_DESCR_FIELD_TEXT'] = 'Название компании';

